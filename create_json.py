#! /usr/bin/env python3

import pandas as pd

df = pd.read_csv('commits.csv')
df['author_date'] = df['author_date'].astype('datetime64[ns]')
months_df = df.groupby([pd.Grouper(key='author_date', freq='M'), 'author_name']).count().reset_index()
#months_df['author'] = months_df['author_name'].str[:8]
months_df['author'] = months_df['author_name']
export_df = months_df[['author_date', 'author', 'hash']]
export_df.columns = ['date', 'author', 'commits']
export_df['date'] = export_df['date'].dt.strftime('%Y-%m')
print(export_df)
export_df.to_json('commits.json', orient='records', date_format='iso')
