#!/usr/bin/python3

"""
Analyze a git repo using Perceval, and producing a JSON file
with the number of commits per committer per month.
"""

import argparse
import datetime
import json
import path
import system
from perceval.backends.core.git import Git

parser = argparse.ArgumentParser(description = "Count commits in a git repo")
parser.add_argument("repo", help = "Repository url")
parser.add_argument("dir", help = "Directory for cloning the repository")
parser.add_argument("--pretty", action='store_true', help = "Pretty print JSON output")
parser.add_argument("--reuse_clon", action='store_true', help = "Reuse directory for cloning, if it exists")
args = parser.parse_args()

if path.exists(args.dir) and not args.reuse_clon:
    print(f"Directory for cloning ({args.dir}) exists, exiting. Use --reuse_clon for reusing it")
    system.exit()
else:
    repo = Git(uri=args.repo, gitpath=args.dir)
count = 0

# Dictionary of dates (of the form 'yyyyy-mm')
# with values formed by dictionaires keyed by authors,
# and for each author, the number of commits
commits = {}

for commit in repo.fetch():
    author = commit['data']['Author']
    name = author.split('<')[0].rstrip()
    authordate = commit['data']['AuthorDate']
    date = datetime.datetime.strptime(authordate, "%a %b %d %H:%M:%S %Y %z")
    date_str = f"{date:%Y-%m}"
    commits[date_str] = commits.get(date_str, {})
    commits[date_str][name] = commits[date_str].get(name, 0) + 1
    count += 1

# print("Number of commmits: %d." % count)

# Prepare the list to produce the final JSON string
list = []
for date, authors in sorted(commits.items()):
    for author, number in sorted(authors.items(), key=lambda author: author[1], reverse=True):
        item = {'date': date,
                'author': author,
                'commits': number}
        list.append(item)

if args.pretty:
    output = json.dumps(list, indent=4)
else:
    output = json.dumps(list)
print(output)